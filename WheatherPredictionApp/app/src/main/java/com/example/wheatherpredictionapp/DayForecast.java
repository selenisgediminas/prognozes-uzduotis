package com.example.wheatherpredictionapp;

public class DayForecast {
    long dayTimeUnix;
    double tempDay;
    double tempNight;
    public DayForecast(long dayTimeUnix,double tempDay,double tempNight){
        this.dayTimeUnix=dayTimeUnix;
        this.tempDay=tempDay;
        this.tempNight=tempNight;
    }

    public long getDayTimeUnix() {
        return dayTimeUnix;
    }

    public void setDayTimeUnix(long dayTimeUnix) {
        this.dayTimeUnix = dayTimeUnix;
    }

    public double getTempDay() {
        return tempDay;
    }

    public void setTempDay(double tempDay) {
        this.tempDay = tempDay;
    }

    public double getTempNight() {
        return tempNight;
    }

    public void setTempNight(double tempNight) {
        this.tempNight = tempNight;
    }
}
