package com.example.wheatherpredictionapp;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private final String countryCode= "440";
    private  final String urlDayly = "https://api.openweathermap.org/data/2.5/onecall";
    private final String appKey = "5dc66f550d3a29cb10bd41041a0bc646";
    private String City = "Vilnius";
    private final double latVilnius = 54.6872;
    private final double longVilnius = 25.2797;
    private final double latKaunas = 54.8985;
    private final double longKaunas = 23.9036;
    private final double latKlaip = 55.7033;
    private final double longKlaip = 21.1443;
    private DecimalFormat df = new DecimalFormat("##.##");
    private DecimalFormat df2 = new DecimalFormat("##.####");

    private Spinner citySpinner;
    private TextView dienosTempText1,naktiesTempText1,dienaText1;
    private TextView dienosTempText2,naktiesTempText2,dienaText2;
    private TextView dienosTempText3,naktiesTempText3,dienaText3;
    private TextView dienosTempText4,naktiesTempText4,dienaText4;
    private TextView dienosTempText5,naktiesTempText5,dienaText5;
    private TextView dienosTempText6,naktiesTempText6,dienaText6;
    private TextView dienosTempText7,naktiesTempText7,dienaText7;

    private ArrayList<DayForecast> weekForecast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        citySpinner = findViewById(R.id.citySpinner);

        naktiesTempText1 = findViewById(R.id.naktiesTempText1);
        dienosTempText1 = findViewById(R.id.dienosTempText1);
        dienaText1 = findViewById(R.id.dienaText1);

        naktiesTempText2 = findViewById(R.id.naktiesTempText2);
        dienosTempText2 = findViewById(R.id.dienosTempText2);
        dienaText2 = findViewById(R.id.dienaText2);

        naktiesTempText3 = findViewById(R.id.naktiesTempText3);
        dienosTempText3 = findViewById(R.id.dienosTempText3);
        dienaText3 = findViewById(R.id.dienaText3);

        naktiesTempText4 = findViewById(R.id.naktiesTempText4);
        dienosTempText4 = findViewById(R.id.dienosTempText4);
        dienaText4 = findViewById(R.id.dienaText4);

        naktiesTempText5 = findViewById(R.id.naktiesTempText5);
        dienosTempText5 = findViewById(R.id.dienosTempText5);
        dienaText5 = findViewById(R.id.dienaText5);

        naktiesTempText6 = findViewById(R.id.naktiesTempText6);
        dienosTempText6 = findViewById(R.id.dienosTempText6);
        dienaText6 = findViewById(R.id.dienaText6);

        naktiesTempText7 = findViewById(R.id.naktiesTempText7);
        dienosTempText7 = findViewById(R.id.dienosTempText7);
        dienaText7 = findViewById(R.id.dienaText7);

        weekForecast = new ArrayList<DayForecast>();
        String[] cityArray = {"Vilnius", "Kaunas", "Klaipėda"};
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cityArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(adapter);

        getDailyWheather();

    }
//https://api.openweathermap.org/data/2.5/onecall?lat=54.6872&lon=25.2797&exclude=hourly,minutely,alerts,current&appid=5dc66f550d3a29cb10bd41041a0bc646
    public void getDailyWheather(){
        String tempUrl = "";
        weekForecast = new ArrayList<DayForecast>();
        switch(City) {
            case "Vilnius":
                tempUrl = urlDayly + "?lat="+ df2.format(latVilnius)+ "&lon="+df2.format(longVilnius)+"&exclude=hourly,minutely,alerts,current&appid="+appKey;
                break;
            case "Kaunas":
                tempUrl = urlDayly + "?lat="+ df2.format(latKaunas)+ "&lon="+df2.format(longKaunas)+"&exclude=hourly,minutely,alerts,current&appid="+appKey;
                break;
            case "Klaipėda":
                tempUrl = urlDayly + "?lat="+ df2.format(latKlaip)+ "&lon="+df2.format(longKlaip)+"&exclude=hourly,minutely,alerts,current&appid="+appKey;
                break;
            default:
                tempUrl = urlDayly + "?lat="+ df2.format(latVilnius)+ "&lon="+df2.format(longVilnius)+"&exclude=hourly,minutely,alerts,current&appid="+appKey;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, tempUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response",response);
                String output ="";
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    JSONArray jsonArray = jsonResponse.getJSONArray("daily");
                    for(int i = 0;i<7;i++){
                        JSONObject jsonObjectWeather = jsonArray.getJSONObject(i);
                        long dt = jsonObjectWeather.getLong("dt");
                        JSONObject jsonArrayTemp = jsonObjectWeather.getJSONObject("temp");
                        double tempDay = jsonArrayTemp.getDouble("day") - 273.15;
                        double tempNight = jsonArrayTemp.getDouble("night") - 273.15;
                        DayForecast forecast = new DayForecast(dt,tempDay,tempNight);
                        weekForecast.add(forecast);
                    }
                    setWeather();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.toString().trim(),Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public String getDateString(long dt){
        Date date = new Date(dt*1000L);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("MM-dd");
        jdf.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        String java_date = jdf.format(date);
        return java_date;
    }
    public void setWeather(){
        int i = 0;
        for (DayForecast dayForecast:weekForecast){
            switch(i) {
                case 0:
                    dienaText1.setText(getDateString(dayForecast.getDayTimeUnix()));
                    dienosTempText1.setText(df.format(dayForecast.getTempDay()));
                    naktiesTempText1.setText(df.format(dayForecast.getTempNight()));
                    break;
                case 1:
                    dienaText2.setText(getDateString(dayForecast.getDayTimeUnix()));
                    dienosTempText2.setText(df.format(dayForecast.getTempDay()));
                    naktiesTempText2.setText(df.format(dayForecast.getTempNight()));
                    break;
                case 2:
                    dienaText3.setText(getDateString(dayForecast.getDayTimeUnix()));
                    dienosTempText3.setText(df.format(dayForecast.getTempDay()));
                    naktiesTempText3.setText(df.format(dayForecast.getTempNight()));
                    break;
                case 3:
                    dienaText4.setText(getDateString(dayForecast.getDayTimeUnix()));
                    dienosTempText4.setText(df.format(dayForecast.getTempDay()));
                    naktiesTempText4.setText(df.format(dayForecast.getTempNight()));
                    break;
                case 4:
                    dienaText5.setText(getDateString(dayForecast.getDayTimeUnix()));
                    dienosTempText5.setText(df.format(dayForecast.getTempDay()));
                    naktiesTempText5.setText(df.format(dayForecast.getTempNight()));
                    break;
                case 5:
                    dienaText6.setText(getDateString(dayForecast.getDayTimeUnix()));
                    dienosTempText6.setText(df.format(dayForecast.getTempDay()));
                    naktiesTempText6.setText(df.format(dayForecast.getTempNight()));
                    break;
                case 6:
                    dienaText7.setText(getDateString(dayForecast.getDayTimeUnix()));
                    dienosTempText7.setText(df.format(dayForecast.getTempDay()));
                    naktiesTempText7.setText(df.format(dayForecast.getTempNight()));
                    break;
                default:
                    // code block
            }
            i++;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(parent.getId() == R.id.citySpinner){
            City = parent.getItemAtPosition(position).toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}